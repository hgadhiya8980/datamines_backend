import re, os, json, time
from flask import (Flask, render_template, request, session, send_file, jsonify, redirect)
from flask_cors import CORS
import jwt
from werkzeug.security import generate_password_hash, check_password_hash
from common_func import logger_con, sending_email_mail, get_data_from_table, validate_phone_number, password_validation, get_error_msg, get_response_msg, get_timestamp, clean_text
from mongo_connection import (mongo_connect, data_added, find_all_data, find_spec_data, update_mongo_data)
from constant import constant_data
from functools import wraps
import random
import concurrent.futures
from PIL import Image
from IPython.display import Image as IPImage
from dotenv import load_dotenv
import os
import google.generativeai as genai
import re

app = Flask(__name__)

CORS(app)

app.config["enviroment"] = "qa"
app.config["SECRET_KEY"] = "sdfsf65416534sdfsdf4653"

UPLOAD_FOLDER = 'static/uploads/'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

app.config['MAIL_SERVER'] = constant_data["mail_configuration"].get("server_host")
app.config['MAIL_PORT'] = int(constant_data["mail_configuration"].get("server_port"))
app.config['MAIL_USERNAME'] = constant_data["mail_configuration"].get("server_username")
app.config['MAIL_PASSWORD'] = constant_data["mail_configuration"].get("server_password")
app.config["all_output"] = []

# handling our application secure type like http or https
secure_type = "http"

load_dotenv()

# logger & MongoDB connection
# logger_con(app=app)
client = mongo_connect(app=app)

GOOGLE_API_KEY = os.getenv("GOOGLE_API_KEY")
genai.configure(api_key=GOOGLE_API_KEY)

model = genai.GenerativeModel('models/gemini-1.0-pro-vision-latest')

ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif' 'svg'}

def allowed_photos(filename):
    """
    checking file extension is correct or not

    :param filename: file name
    :return: True, False
    """
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def token_required(func):
    # decorator factory which invoks update_wrapper() method and passes decorated function as an argument
    @wraps(func)
    def decorated(*args, **kwargs):
        login_dict = session.get("login_dict", "available")
        # token = app.config["mapping_user_dict"].get(login_dict.get("id", "nothing"), {}).get("token", False)
        if login_dict == "available":
            app.logger.debug("please first login in your app...")
        return func(*args, **kwargs)
    return decorated

def get_response_image_data(filename):
    """_summary_
    find output like color,  category and pattern for given image
    Args:
        image_path (_type_): relative path of image
    """
    try:
        image_path = filename

        # Save the PIL image to a local file
        pil_image = Image.open(image_path)
        pil_image.save(filename)

        # Create IPython Image object
        img = IPImage(filename=image_path)

        response = model.generate_content(['Provide the output for given format like {"category_name":"category_name", "pattern_name": "pattern_name", "color_code": "color_code", "summury": "summary", "gender": "gender"} based on given input fashion product image. The values for these keys should correspond to the details of a given fashion product image. Additionally, please specify whether the product is intended for men, women, or is unisex.also make summury in one line. Please note I do not require any text in output.', img])

        try:
            response_text = json.loads(response.text)
        except:
            response_text = response.text

        return response_text

    except Exception as e:
        print(f"Error in get response in filename: {e}")


def get_response(filename, filetextname):
    """
    find output like color,  category and pattern for given image
    Args:
        image_path (_type_): relative path of image
    """
    try:
        # Save the PIL image to a local file
        pil_image = Image.open(filename)
        pil_image.save(filename)

        # Create IPython Image object
        img = IPImage(filename=filename)

        response = model.generate_content(["providing the output in the same language. Extract the following entities and keys like [person_names, designations, contact_numbers, website_urls, locations, services, email_addresses, and company_names from the given content text='{all_content}'  Provide the output in JSON format only. No additional information is required, only the JSON output.", img])
        response.resolve()
        response_text = response.text
        output = {}
        app.logger.debug(f"response text from image: {response_text}")
        try:
            json_data = re.search(r'\{.*\}', response_text, re.DOTALL).group(0)
            output = json.loads(json_data)
            output["filename"] = filetextname
        except Exception as e:
            output["filename"] = filetextname
            pass
        print(output)
        app.config["all_output"].append(output)

    except Exception as e:
        print(f"Error in get response in filename: {e}")

@app.route("/cardapi/v1/register", methods=["GET", "POST"])
def register():
    """
    In this route we can handling student register process
    :return: register template
    """
    try:
        if request.method == "POST":
            app.logger.debug("coming in register api")
            company_name = request.form["company_name"]
            person_name = request.form["person_name"]
            username = request.form["username"]
            business_type = request.form["business_type"]
            password = request.form["password"]
            city = request.form["city"]
            state = request.form["state"]
            country = request.form["country"]
            pincode = request.form["pincode"]
            contact_no = request.form["contact_no"]
            email = request.form["email"]
            type = request.form["type"]

            user_id = request.args.get("user_id", "")

            spliting_email = email.split("@")[-1]
            if "." not in spliting_email:
                response_data = get_response_msg(app, "FAIL", 400, "Email is not valid", {})
                return response_data

            user_data = get_data_from_table(app, client, "user_data")
            get_all_username = [data["username"] for data in user_data]
            if username in get_all_username:
                response_data = get_response_msg(app, "FAIL", 400, "Username already exits...", {})
                return response_data

            user_data = get_data_from_table(app, client, "user_data")
            get_all_contact = [data["contact_no"] for data in user_data]
            if contact_no in get_all_contact:
                response_data = get_response_msg(app, "FAIL", 400, "Contact-no already exits...", {})
                return response_data

            # password validation
            if not password_validation(app=app, password=password):
                response_data = get_response_msg(app, "FAIL", 400, "Please choose strong password. Add at least 1 special character, number, capitalize latter..", {})
                return response_data
            
            # contact number and emergency contact number validation
            # get_phone_val = validate_phone_number(app=app, phone_number=contact_no)
            # if get_phone_val == "invalid number":
            #     response_data = get_response_msg(app, "FAIL", 400, "Phone number is not valid...", {})
            #     return response_data

            register_dict = {
                "user_id": user_id,
                "company_name": company_name,
                "business_type": business_type,
                "username": username,
                "person_name": person_name,
                "password": password,
                "contact_no": contact_no,
                "email": email,
                "city": city,
                "state": state,
                "country": country,
                "pincode": pincode,
                "type": type,
                "status": "deactivate",
                "inserted_on": get_timestamp(app),
                "updated_on": get_timestamp(app)
            }

            db = client["card_extractor"]
            alluserdata = find_all_data(app, db, "user_data")
            alluserid = [var["user_id"] for var in alluserdata]

            if user_id in alluserid:
                response_data = get_response_msg(app, "FAILURE", 403, "User ID already exits...", {})
                return response_data

            db = client["card_extractor"]
            data_added(app, db, "user_data", register_dict)
            response_data = get_response_msg(app, "REGISTER", 200, "Register Successfully...", {})
            return response_data
        else:
            response_data = get_response_msg(app, "REGISTER", 500, "Please use post method", {})
            return response_data

    except Exception as e:
        response_data = get_error_msg(app, e)
        app.logger.debug(f"Error in add student data route: {e}")
        return response_data

@app.route("/cardapi/v1/login", methods=["GET", "POST"])
def login():
    """
    In this route we can handling student, teacher and admin login process
    :return: login template
    """
    try:
        db = client["card_extractor"]
        app.logger.debug("coming in login api")
        if request.method == "POST":
            condition = request.form["condition"]
            password = request.form["password"]
            app.logger.debug("coming in post method login api")

            di_email = {"email": condition}
            di_contact = {"contact_no": condition}
            email_data = find_spec_data(app, db, "user_data", di_email)
            contact_data = find_spec_data(app, db, "user_data", di_contact)
            email_data = list(email_data)
            contact_data = list(contact_data)

            if len(email_data) == 0 and len(contact_data) == 0:
                response_data = get_response_msg(app, "FAILURE", 403, "User not exits...", {})
                return response_data
            elif len(email_data)>0:
                email_data = email_data[0]
                user_id = email_data["user_id"]
                email = email_data["email"]

                if email_data["password"] == password:
                    token = jwt.encode({'email': email}, app.config['SECRET_KEY'], algorithm='HS256')
                    response_data = get_response_msg(app, "SUCCESS", 200, "Login successfully",
                                                     {"email": email, "user_id": user_id, "type": email_data["type"], "token": token})
                else:
                    response_data = get_response_msg(app, "FAILURE", 403, "Please use correct credential", {"email": condition, "user_id": user_id})
                return response_data

            elif len(contact_data) > 0:
                contact_data = contact_data[0]
                user_id = contact_data["user_id"]
                email = contact_data["email"]

                if contact_data["password"] == password:
                    token = jwt.encode({'email': email}, app.config['SECRET_KEY'], algorithm='HS256')
                    response_data = get_response_msg(app, "SUCCESS", 200, "Login successfully",
                                                     {"email": email, "user_id": user_id, "type": contact_data["type"], "token": token})
                else:
                    response_data = get_response_msg(app, "FAILURE", 403, "Please use correct credential", {"email": condition})
                return response_data
        else:
            response_data = get_response_msg(app, "REGISTER", 500, "Please use post method", {})
            return response_data

    except Exception as e:
        response_data = get_error_msg(app, e)
        return response_data

@app.route("/cardapi/v1/otp_verification", methods=["GET", "POST"])
def otp_verification():
    """
    That funcation can use otp_verification and new_password set link generate
    """

    try:
        if request.method == "POST":
            app.logger.debug("coming in otp verification api")
            email = request.args.get("email", "")
            entered_otp = request.form["otp"]
            entered_otp = int(entered_otp)
            mail_otp = request.args.get("mail_otp")
            mail_otp = int(mail_otp)
            if entered_otp==mail_otp:
                token = jwt.encode({'email': email}, app.config['SECRET_KEY'], algorithm='HS256')
                response_data = get_response_msg(app, "SUCCESS", 200, "Login successfully",
                                                 {"email":email, "token": token})
                return response_data
            else:
                response_data = get_response_msg(app, "FAILURE", 403, "Wrong OTP",
                                                 {"email": email})
                return response_data
        else:
            email = request.args.get("email", "")
            otp = random.randint(100000, 999999)
            server_host = app.config['MAIL_SERVER']
            server_port = app.config['MAIL_PORT']
            server_username = app.config['MAIL_USERNAME']
            server_password = app.config['MAIL_PASSWORD']
            subject_title = "OTP Received"
            mail_format = f"Hello There,\n We hope this message finds you well. As part of our ongoing commitment to ensure the security of your account, we have initiated a verification process.\nYour One-Time Password (OTP) for account verification is: [{otp}]\nPlease enter this OTP on the verification page to complete the process. Note that the OTP is valid for a limited time, so we recommend entering it promptly.\nIf you did not initiate this verification or have any concerns regarding your account security, please contact our support team immediately at help@codescatter.com\n\nThank you for your cooperation.\nBest regards,\nCodescatter"
            html_format = f'<p>Hello There,</p><p>We hope this message finds you well. As part of our ongoing commitment to ensure the security of your account, we have initiated a verification process.</p><p>Your One-Time Password (OTP) for account verification is: <h2><b>{otp}</h2></b></p><p>Please enter this OTP on the verification page to complete the process. Note that the OTP is valid for a limited time, so we recommend entering it promptly.</p><p>If you did not initiate this verification or have any concerns regarding your account security, please contact our support team immediately at help@codescatter.com</p><p>Thank you for your cooperation.</p><p>Best regards,<br>Codescatter</p>'
            sending_email_mail(app, [email], subject_title, mail_format, html_format, server_username,
                               server_password, server_host, int(server_port))
            response_data = get_response_msg(app, "SUCCESS", 200, "OTP sent successfully", {"email": email, "otp": otp})
            return response_data

    except Exception as e:
        response_data = get_error_msg(app, e)
        return response_data

@app.route("/cardapi/v1/forgot_password", methods=["GET", "POST"])
def forgot_password():
    """
    Handling teacher register process
    :return: teacher register template
    """
    try:
        db = client["card_extractor"]
        if request.method == "POST":
            app.logger.debug("coming in forgot password api")
            email = request.form["email"]
            all_login_data = find_spec_data(app, db, "user_data", {"email": email})
            all_login_data = list(all_login_data)
            if len(all_login_data)==0:
                response_data = get_response_msg(app, "FAILURE", 403, "Record not found",
                                                 {"email": email})
                return response_data
            else:
                uuid = all_login_data[0]["user_id"]
                server_host = app.config['MAIL_SERVER']
                server_port = app.config['MAIL_PORT']
                server_username = app.config['MAIL_USERNAME']
                server_password = app.config['MAIL_PASSWORD']
                subject_title = "Reset Your Password"
                mail_format = f"Hello There,\n I hope this email finds you well. It has come to our attention that you have requested to reset your password for your APPIACS account. If you did not initiate this request, please disregard this email.\nTo reset your password,\nplease follow the link below: \nClick Here \nPlease note that this link is valid for the next 30 Minutes. After this period, you will need to submit another password reset request.\nIf you continue to experience issues or did not request a password reset, please contact our support team for further assistance.\nThank you for using Website.\n\nBest regards,\nHarshit Gadhiya"
                html_format = f"<p>Dear customer,<br><br>It seems you've forgotten your Krazio Cloud account password. No worries!<br><br>Please click the link below to reset your password: <br><br><a href='https://datacraft-ashy.vercel.app/ResetPassword?uuid={uuid}'><b>Click Here</b></a><br><br>If you did not request this password reset, please ignore this email.<br><br>Best regards,<br>The Krazio Cloud Team</p>"
                sending_email_mail(app, [email], subject_title, mail_format, html_format, server_username,
                                   server_password, server_host, int(server_port))
                response_data = get_response_msg(app, "SUCCESS", 200, "Sent reset password",
                                                 {"email": email})
                return response_data
        else:
            response_data = get_response_msg(app, "FAILURE", 500, "Please use post method", {})
            return response_data

    except Exception as e:
        response_data = get_error_msg(app, e)
        return response_data

@app.route("/cardapi/v1/reset_password", methods=["GET", "POST"])
def reset_password():
    """
    Handling teacher register process
    :return: teacher register template
    """
    try:
        db = client["card_extractor"]
        if request.method == "POST":
            app.logger.debug("coming in reset password api")
            user_id = request.args.get("uuid", "")
            password = request.form["password"]
            confirm_password = request.form["confirm_password"]
            if password == confirm_password:
                condition = {"user_id": user_id}
                password_new = generate_password_hash(password)
                update_mongo_data(app, db, "user_data", condition, {"password": password})
                response_data = get_response_msg(app, "SUCCESS", 200, "Update password successfully...",
                                                 {"user_id": user_id})
                return response_data
            else:
                response_data = get_response_msg(app, "FAILURE", 403, "password does not match",
                                                 {"user_id": user_id})
                return response_data
        else:
            response_data = get_response_msg(app, "FAILURE", 500, "Please use post method", {})
            return response_data

    except Exception as e:
        response_data = get_error_msg(app, e)
        return response_data

@app.route("/", methods=["GET", "POST"])
def home():
    """
    Handling teacher register process
    :return: teacher register template
    """
    return {"data": "success"}

@app.route('/logout', methods=['GET', 'POST'])
def logout():
    """
    That funcation was logout session and clear user session
    """

    try:
        session.clear()
        app.logger.debug(f"session is {session}")
        response_data = get_response_msg(app, "LOGOUT", 200, "logout successfully", {})
        return response_data

    except Exception as e:
        response_data = get_error_msg(app, e)
        app.logger.debug(f"error is {e}")
        return response_data

@app.route('/cardapi/v1/get_text_extract', methods=['GET', 'POST'])
def get_text_extract():
    """
    That funcation was logout session and clear user session
    """

    try:
        user_id = request.args.get("user_id")
        app.config["all_output"] = []
        token = request.headers.get('Authorization').split(" ")[1]  # Assuming the token is passed as "Bearer <token>"
        app.logger.debug(f"token in get extract api: {token}")
        try:
            decoded_token = jwt.decode(token, app.config['SECRET_KEY'], algorithms=['HS256'])
        except jwt.ExpiredSignatureError:
            return jsonify({'message': 'Access Token is expired'}), 401
        except jwt.InvalidTokenError:
            return jsonify({'message': 'Invalid token'}), 401

        if request.method=="POST":
            attachment_file = request.files.getlist('attachment_file')
            attach_save_dir = os.path.abspath("static/upload/")

            attachment_all_file = []
            all_output = []
            all_filename = []
            if attachment_file:
                for file in attachment_file:
                    if file.filename != '':
                        attach_file_path = os.path.join(attach_save_dir, file.filename)
                        attachment_all_file.append(attach_file_path)
                        all_filename.append(file.filename)
                        file.save(attach_file_path)

            executor = concurrent.futures.ThreadPoolExecutor(max_workers=5)
            threads = []
            for attachment_path, filename in zip(attachment_all_file,all_filename):
                app.logger.debug(f"attachment path is {attachment_path}")
                threads.append(executor.submit(get_response, attachment_path, filename))

            # Wait for all threads to finish
            concurrent.futures.wait(threads)

            all_output = {"output_data": app.config["all_output"]}
            response_data = jsonify(get_response_msg(app, "SUCCESS", 200, "Extracted text successfully", all_output))
            return response_data
        else:
            response_data = get_response_msg(app, "FAILURE", 500, "Please use post method", {})
            return response_data

    except Exception as e:
        response_data = get_error_msg(app, e)
        return response_data

@app.route("/cardapi/v1/file_store", methods=["GET", "POST"])
def file_store():
    """
    In this route we can handling student register process
    :return: register template
    """
    try:
        token = request.headers.get('Authorization').split(" ")[1]  # Assuming the token is passed as "Bearer <token>"
        app.logger.debug(f"token in file store api: {token}")
        try:
            decoded_token = jwt.decode(token, app.config['SECRET_KEY'], algorithms=['HS256'])
        except jwt.ExpiredSignatureError:
            return jsonify({'message': 'Access Token is expired'}), 401
        except jwt.InvalidTokenError:
            return jsonify({'message': 'Invalid token'}), 401

        if request.method == "POST":
            app.logger.debug("coming in file store api")
            user_id = request.form["user_id"]
            file_name_excel = request.form["file_name_excel"]
            file_url_excel = request.form["file_url_excel"]
            file_name_pdf = request.form["file_name_pdf"]
            file_url_pdf = request.form["file_url_pdf"]

            db = client["card_extractor"]
            allrecorddata = find_all_data(app, db, "file_data")
            allrecordid = [var["record_id"] for var in allrecorddata]

            record_id = ""
            flag = True
            while flag:
                record_id = random.randint(100000000000, 999999999999)
                if str(record_id) not in allrecordid:
                    flag = False

            register_dict = {
                "user_id": user_id,
                "record_id": str(record_id),
                "file_name_excel": file_name_excel,
                "file_url_excel": file_url_excel,
                "file_name_pdf": file_name_pdf,
                "file_url_pdf": file_url_pdf,
                "status": "true",
                "inserted_on": get_timestamp(app)
            }

            db = client["card_extractor"]
            data_added(app, db, "file_data", register_dict)
            response_data = get_response_msg(app, "SUCCESS", 200, "File store Successfully...", {})
            return response_data
        else:
            response_data = get_response_msg(app, "REGISTER", 500, "Please use post method", {})
            return response_data

    except Exception as e:
        response_data = get_error_msg(app, e)
        app.logger.debug(f"Error in add student data route: {e}")
        return response_data

@app.route("/cardapi/v1/get_user_files", methods=["GET", "POST"])
def get_user_files():
    """
    In this route we can handling student register process
    :return: register template
    """
    try:
        token = request.headers.get('Authorization').split(" ")[1]  # Assuming the token is passed as "Bearer <token>"
        app.logger.debug(f"token in file store api: {token}")
        try:
            decoded_token = jwt.decode(token, app.config['SECRET_KEY'], algorithms=['HS256'])
        except jwt.ExpiredSignatureError:
            return jsonify({'message': 'Access Token is expired'}), 401
        except jwt.InvalidTokenError:
            return jsonify({'message': 'Invalid token'}), 401

        user_id = request.args.get("user_id", "")
        record_id = request.args.get("record_id", "")
        file_status = request.args.get("file_status", "")
        condition_dict = {}
        if user_id:
            condition_dict["user_id"] = user_id

        if file_status:
            condition_dict["status"] = file_status

        if record_id:
            condition_dict["record_id"] = record_id

        db = client["card_extractor"]
        all_user_data = find_spec_data(app, db, "file_data", condition_dict)
        all_user_list = []
        for var in all_user_data:
            del var["_id"]
            del var["user_id"]
            all_user_list.append(var)

        response_data = get_response_msg(app, "SUCCESS", 200, "Get File Successfully...", {"files_data": all_user_list})
        return response_data

    except Exception as e:
        response_data = get_error_msg(app, e)
        app.logger.debug(f"Error in add student data route: {e}")
        return response_data

@app.route("/cardapi/v1/delete_file", methods=["GET", "POST"])
def delete_file():
    """
    In this route we can handling student register process
    :return: register template
    """
    try:
        token = request.headers.get('Authorization').split(" ")[1]  # Assuming the token is passed as "Bearer <token>"
        app.logger.debug(f"token in file store api: {token}")
        try:
            decoded_token = jwt.decode(token, app.config['SECRET_KEY'], algorithms=['HS256'])
        except jwt.ExpiredSignatureError:
            return jsonify({'message': 'Access Token is expired'}), 401
        except jwt.InvalidTokenError:
            return jsonify({'message': 'Invalid token'}), 401

        user_id = request.args.get("user_id", "")
        record_id = request.args.get("record_id", "")
        file_status = request.args.get("type", "delete")
        condition_dict = {}
        if user_id:
            condition_dict["user_id"] = user_id

        if record_id:
            condition_dict["record_id"] = record_id

        db = client["card_extractor"]
        if file_status == "delete":
            update_mongo_data(app, db, "file_data", condition_dict, {"status": "false"})
            response_data = get_response_msg(app, "SUCCESS", 200, "File Deleted Successfully...", {})
        else:
            update_mongo_data(app, db, "file_data", condition_dict, {"status": "true"})
            response_data = get_response_msg(app, "SUCCESS", 200, "File Recover Successfully...", {})
        return response_data

    except Exception as e:
        response_data = get_error_msg(app, e)
        app.logger.debug(f"Error in add student data route: {e}")
        return response_data

@app.route("/cardapi/v1/get_all_user", methods=["GET", "POST"])
def get_users():
    """
    In this route we can handling student register process
    :return: register template
    """
    try:
        token = request.headers.get('Authorization').split(" ")[1]  # Assuming the token is passed as "Bearer <token>"
        app.logger.debug(f"token in file store api: {token}")
        try:
            decoded_token = jwt.decode(token, app.config['SECRET_KEY'], algorithms=['HS256'])
        except jwt.ExpiredSignatureError:
            return jsonify({'message': 'Access Token is expired'}), 401
        except jwt.InvalidTokenError:
            return jsonify({'message': 'Invalid token'}), 401

        user_id = request.args.get("user_id", "")
        file_status = request.args.get("file_status", "")
        condition_dict = {}
        if user_id:
            condition_dict["user_id"] = user_id

        db = client["card_extractor"]
        all_user_data = find_spec_data(app, db, "user_data", condition_dict)
        all_user_list = []
        for var in all_user_data:
            del var["_id"]
            collections = db["file_data"]
            num_records = collections.count_documents({"user_id": var["user_id"], "status": file_status})
            var["count"] = num_records
            all_user_list.append(var)

        response_data = get_response_msg(app, "SUCCESS", 200, "Get User Successfully...", {"user_data": all_user_list})
        return response_data

    except Exception as e:
        response_data = get_error_msg(app, e)
        app.logger.debug(f"Error in add student data route: {e}")
        return response_data

@app.route("/cardapi/v1/user_activate", methods=["GET", "POST"])
def user_activate():
    """
    In this route we can handling student register process
    :return: register template
    """
    try:
        token = request.headers.get('Authorization').split(" ")[1]  # Assuming the token is passed as "Bearer <token>"
        app.logger.debug(f"token in file store api: {token}")
        try:
            decoded_token = jwt.decode(token, app.config['SECRET_KEY'], algorithms=['HS256'])
        except jwt.ExpiredSignatureError:
            return jsonify({'message': 'Access Token is expired'}), 401
        except jwt.InvalidTokenError:
            return jsonify({'message': 'Invalid token'}), 401

        user_id = request.args.get("user_id", "")
        status = request.args.get("status", "deactivate")
        condition_dict = {}
        if user_id:
            condition_dict["user_id"] = user_id

        db = client["card_extractor"]
        update_mongo_data(app, db, "user_data", condition_dict, {"status": status})

        response_data = get_response_msg(app, "SUCCESS", 200, "User status updated Successfully...", {})
        return response_data

    except Exception as e:
        response_data = get_error_msg(app, e)
        app.logger.debug(f"Error in add student data route: {e}")
        return response_data

@app.route("/cardapi/v1/delete_user", methods=["GET", "POST"])
def delete_user():
    """
    In this route we can handling student register process
    :return: register template
    """
    try:
        token = request.headers.get('Authorization').split(" ")[1]  # Assuming the token is passed as "Bearer <token>"
        app.logger.debug(f"token in file store api: {token}")
        try:
            decoded_token = jwt.decode(token, app.config['SECRET_KEY'], algorithms=['HS256'])
        except jwt.ExpiredSignatureError:
            return jsonify({'message': 'Access Token is expired'}), 401
        except jwt.InvalidTokenError:
            return jsonify({'message': 'Invalid token'}), 401

        user_id = request.args.get("user_id", "")
        condition_dict = {}
        if user_id:
            condition_dict["user_id"] = user_id

        db = client["card_extractor"]
        coll = db["user_data"]
        coll.delete_many(condition_dict)

        response_data = get_response_msg(app, "SUCCESS", 200, "User Delete Successfully...", {})
        return response_data

    except Exception as e:
        response_data = get_error_msg(app, e)
        app.logger.debug(f"Error in add student data route: {e}")
        return response_data

@app.route('/cardapi/v1/get_image_data', methods=['GET', 'POST'])
def get_image_data():
    """
    That funcation was logout session and clear user session
    """

    try:
        app.config["all_output"] = []
        token = request.headers.get('Authorization').split(" ")[1]  # Assuming the token is passed as "Bearer <token>"
        app.logger.debug(f"token in get extract api: {token}")
        try:
            decoded_token = jwt.decode(token, app.config['SECRET_KEY'], algorithms=['HS256'])
        except jwt.ExpiredSignatureError:
            return jsonify({'message': 'Access Token is expired'}), 401
        except jwt.InvalidTokenError:
            return jsonify({'message': 'Invalid token'}), 401

        if request.method=="POST":
            attachment_file = request.files.getlist('attachment_file')
            attach_save_dir = os.path.abspath("static/upload/")

            attachment_all_file = []
            all_output = []
            all_filename = []
            if attachment_file:
                for file in attachment_file:
                    if file.filename != '':
                        attach_file_path = os.path.join(attach_save_dir, file.filename)
                        attachment_all_file.append(attach_file_path)
                        all_filename.append(file.filename)
                        file.save(attach_file_path)

            for attachment_path, filename in zip(attachment_all_file,all_filename):
                app.logger.debug(f"attachment path is {attachment_path}")
                response_text = get_response_image_data(attachment_path)
                all_output.append(response_text)

            all_output_new = {"output_data": all_output}
            response_data = jsonify(get_response_msg(app, "SUCCESS", 200, "Extracted Data successfully", all_output_new))
            return response_data
        else:
            response_data = get_response_msg(app, "FAILURE", 500, "Please use post method", {})
            return response_data

    except Exception as e:
        response_data = get_error_msg(app, e)
        return response_data

@app.route("/cardapi/v1/data_store", methods=["GET", "POST"])
def data_store():
    """
    In this route we can handling student register process
    :return: register template
    """
    try:
        if request.method == "POST":
            app.logger.debug("coming in data store api")
            company_name = request.form["company_name"]
            username = request.form["username"]
            phone = request.form["phone"]
            email = request.form["email"]

            spliting_email = email.split("@")[-1]
            if "." not in spliting_email:
                response_data = get_response_msg(app, "FAIL", 400, "Email is not valid", {})
                return response_data

            user_data = get_data_from_table(app, client, "data_store")
            get_all_email = [data["email"] for data in user_data]
            if email in get_all_email:
                response_data = get_response_msg(app, "FAIL", 400, "Email already exits...", {})
                return response_data

            register_dict = {
                "company_name": company_name,
                "username": username,
                "phone": phone,
                "email": email,
                "inserted_on": get_timestamp(app),
                "updated_on": get_timestamp(app)
            }

            db = client["card_extractor"]
            data_added(app, db, "data_store", register_dict)
            server_host = app.config['MAIL_SERVER']
            server_port = app.config['MAIL_PORT']
            server_username = app.config['MAIL_USERNAME']
            server_password = app.config['MAIL_PASSWORD']
            subject_title = f"New User Information: {username}"
            mail_format = f"Hello There,\n I hope this email finds you well. It has come to our attention that you have requested to reset your password for your APPIACS account. If you did not initiate this request, please disregard this email.\nTo reset your password,\nplease follow the link below: \nClick Here \nPlease note that this link is valid for the next 30 Minutes. After this period, you will need to submit another password reset request.\nIf you continue to experience issues or did not request a password reset, please contact our support team for further assistance.\nThank you for using Website.\n\nBest regards,\nHarshit Gadhiya"
            html_format = f"<p>I hope this email finds you well. I wanted to bring to your attention the details of a new user who has recently registered with us. Below are the user's information:<br><br><b>Name:</b> {username}<br><b>Email:</b> {email}<br><b>Phone Number:</b> {phone}<br><br>Please review this information at your earliest convenience. If you require any further details or assistance, feel free to reach out to me.<br><br>Thank you for your attention to this matter.<br><br>Best regards,<br>The Krazio Cloud Team</p>"
            sending_email_mail(app, ["sales@kraziocloud.in"], subject_title, mail_format, html_format, server_username,
                               server_password, server_host, int(server_port))
            response_data = get_response_msg(app, "SUCCESS", 200, "Data store Successfully...", {})
            return response_data
        else:
            response_data = get_response_msg(app, "FAILURE", 500, "Please use post method", {})
            return response_data

    except Exception as e:
        response_data = get_error_msg(app, e)
        app.logger.debug(f"Error in add student data route: {e}")
        return response_data

@app.route("/cardapi/v1/get_all_data_store", methods=["GET", "POST"])
def get_all_data_store():
    """
    In this route we can handling student register process
    :return: register template
    """
    try:
        user_id = request.args.get("user_id", "")
        condition_dict = {}
        if user_id:
            condition_dict["user_id"] = user_id

        db = client["card_extractor"]
        all_user_data = find_spec_data(app, db, "data_store", condition_dict)
        all_user_list = []
        for var in all_user_data:
            del var["_id"]
            all_user_list.append(var)

        response_data = get_response_msg(app, "SUCCESS", 200, "Get Data Successfully...", {"user_data": all_user_list})
        return response_data

    except Exception as e:
        response_data = get_error_msg(app, e)
        app.logger.debug(f"Error in add student data route: {e}")
        return response_data
    
    