from PIL import Image
import pytesseract
from langchain.llms import OpenAI
from langchain.prompts import PromptTemplate
import re, os, json, time
from flask import (Flask, render_template, request, session, send_file, jsonify, redirect)
from flask_cors import CORS
from datetime import datetime
import pandas as pd
import logging
import jwt
from werkzeug.security import generate_password_hash, check_password_hash
from pymongo import MongoClient
from flask_mail import Mail, Message

def logger_con(app):
    """
    Configure logger with filename

    :param app: app-name
    :return:
    """
    try:
        server_file_name = "server.log"
        logging.basicConfig(filename=server_file_name, level=logging.DEBUG)

    except Exception as e:
        app.logger.error(f"Error when connecting logger: {e}")

def clean_text(app, text):
    try:
        # Remove extra spaces and newlines
        text = re.sub(r'\n+', '\n', text)
        text = re.sub(r'\s+', ' ', text)
        symbol_pattern = r'[^\w\s.,]'

        # Use re.sub() to replace symbols with spaces
        cleaned_text = re.sub(symbol_pattern, ' ', text)

        return cleaned_text.strip()

    except Exception as e:
        app.logger.debug(f"Error in clean text function: {e}")

def get_timestamp(app):
    try:
        current_datetime = datetime.now()
        formatted_datetime = current_datetime.strftime("%m-%d-%Y %H:%M:%S")

        return formatted_datetime

    except Exception as e:
        app.logger.debug(f"Error in get timestamp: {e}")

def get_response_msg(app, status, statuscode, message, data):
    try:
        response_data_msg = {
            "timestamp": get_timestamp(app),
            "status": status,
            "statusCode": statuscode,
            "message": message,
            "data": data
        }

        return response_data_msg

    except Exception as e:
        app.logger.debug(f"Error in get response msg: {e}")

def get_error_msg(app, e):
    try:
        response_data_msg = {
          "data": "null",
          "message": f"Server Not Responding Error {e}",
          "status": "FORBIDDEN",
          "statusCode": 403,
          "timestamp": get_timestamp(app)
        }

        return response_data_msg

    except Exception as e:
        app.logger.debug(f"Error in get response msg: {e}")




def get_data_from_table(app, client, coll_name):
    try:
        db = client["card_extractor"]
        coll = db[coll_name]
        all_data = coll.find({})
        all_data = list(all_data)
        return all_data

    except Exception as e:
        app.logger.error(f"Error in get data from collection: {e}")


def password_validation(app, password):
    """
    Check password is validate or not

    :param app: app-name
    :param password: user password
    :return: True or False
    """
    try:
        capital = False
        number = False
        special_char = False
        special_char_list = ["@", "#", "$", "-", "_", "*"]
        for char in password:
            if char.isupper():
                capital = True
            try:
                char = int(char)
                number = True
            except:
                pass

            if char in special_char_list:
                special_char = True

        if capital and number and special_char:
            matching = True
        else:
            matching = False

        return matching

    except Exception as e:
        app.logger.debug(f"Error in validate password: {e}")


def validate_phone_number(app, phone_number):
    """
    Check phone number is validate or not

    :param app: app-name
    :param phone_number: user phone-number
    :return: True or False
    """
    try:
        if len(phone_number) <= 14 and len(phone_number) >= 9:
            return "valid number"
        else:
            return "invalid number"

    except Exception as e:
        app.logger.debug(f"Error in validate password: {e}")


def sending_email_mail(app, to_m, subject_main, body_text,html_text, username, password_to, host_main, port_main):
    try:
        app.config['MAIL_SERVER'] = host_main
        app.config['MAIL_PORT'] = port_main
        app.config['MAIL_USE_SSL'] = True
        app.config['MAIL_USERNAME'] = username
        app.config['MAIL_PASSWORD'] = password_to
        mail = Mail(app)

        msg = Message(subject_main, sender=username,
                      recipients=to_m)
        msg.html = html_text

        try:
            mail.send(msg)
            app.logger.debug(f"Email sent successfully!")
        except Exception as e:
            app.logger.debug(f"Error in sending mail function: {e}")

        return "success"

    except Exception as e:
        app.logger.debug(f"Error in sending mail function: {e}")
        return "failure"



